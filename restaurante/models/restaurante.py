from django.db import models;

class Restaurante(models.Model):
    #atributos del producto, que van a representar las columnas de la tabla
    id = models.AutoField(primary_key=True)
    name = models.CharField('NameProduct', max_length = 50)
    price = models.IntegerField(default=0)
    enable = models.BooleanField(default=0)
