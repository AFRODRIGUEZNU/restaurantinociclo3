# se importa el modelo de producto para usarlo en el serializador 
from restaurante.models import restaurante
from restaurante.models.restaurante import Restaurante
# se importa la clase serializers para crear clase serializadora
from rest_framework import serializers
from restaurante.serializers.IngredientesSerializer import IngredientesSerializer

# se crea clase serializadora para poder convertir los datos del modelo a JSON
class RestauranteSerializer(serializers.ModelSerializer):
    class Meta:
        # se asigna el modelo a la clase
        model = Restaurante
        # se eligen cuales campos del modelo se desea mostar
        fields = ['name','price','enable']

        