from django.contrib import admin
from .models.restaurante import Restaurante
from .models.ingredientes import Ingredientes

# se registra el modelo creado para que pueda ser usado por la aplicacion
admin.site.register(Restaurante)
admin.site.register(Ingredientes)
