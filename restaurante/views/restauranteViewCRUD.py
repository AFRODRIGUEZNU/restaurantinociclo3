from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from restaurante.models import Restaurante
from restaurante.serializers import RestauranteSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','POST'])
def restaurante_api_view(request):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        #  usando ORM se realiza la consultaa la base de datos de todos los productos
        restaurante = Restaurante.objects.all()
        # se representa la informacion solicitada en JSON y se marca many=true para asignar anidacion
        restaurante_serializer = RestauranteSerializer(restaurante,many=True)
        # se envida la respuesta serializada que se encuentra en data
        return Response(restaurante_serializer.data)

    elif request.method == 'POST':
        restaurante_serializer = RestauranteSerializer(data = request.data)
        if restaurante_serializer.is_valid():
            restaurante_serializer.save()
            return Response(restaurante_serializer.data)
        return Response(restaurante_serializer.errors)


@api_view(['GET','PUT','DELETE'])
def restaurante_detail_view(request,pk=None):

    if request.method == 'GET':
        restaurante = Restaurante.objects.filter(id = pk).first()
        restaurante_serializer = RestauranteSerializer(restaurante)
        return Response(restaurante_serializer.data)
    
    elif request.method == 'PUT':
        restaurante = Restaurante.objects.filter(id = pk).first()
        restaurante_serializer = RestauranteSerializer(data = request.data)
        if restaurante_serializer.is_valid():
            restaurante_serializer.save()
            return Response(restaurante_serializer.data)
        return Response(restaurante_serializer.errors)
    
    elif request.method == 'DELETE':
        restaurante = Restaurante.objects.filter(id = pk).first()
        restaurante.delete()
        return Response("El plato del menu ha sido eliminado.")