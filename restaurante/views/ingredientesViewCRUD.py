from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from restaurante.models import Ingredientes
from restaurante.serializers import IngredientesSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','POST'])
def ingredientes_api_view(request):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        #  usando ORM se realiza la consultaa la base de datos de todos los productos
        ingredientes = Ingredientes.objects.all()
        # se representa la informacion solicitada en JSON y se marca many=true para asignar anidacion
        ingredientes_serializer = IngredientesSerializer(ingredientes,many=True)
        # se envida la respuesta serializada que se encuentra en data
        return Response(ingredientes_serializer.data)

    elif request.method == 'POST':
        ingredientes_serializer = IngredientesSerializer(data = request.data)
        if ingredientes_serializer.is_valid():
            ingredientes_serializer.save()
            return Response(ingredientes_serializer.data)
        return Response(ingredientes_serializer.errors)


@api_view(['GET','PUT','DELETE'])
def ingredientes_detail_view(request,pk=None):

    if request.method == 'GET':
        ingredientes = Ingredientes.objects.filter(id = pk).first()
        ingredientes_serializer = IngredientesSerializer(ingredientes)
        return Response(ingredientes_serializer.data)
    
    elif request.method == 'PUT':
        ingredientes = Ingredientes.objects.filter(id = pk).first()
        ingredientes_serializer = IngredientesSerializer(ingredientes)
        if ingredientes_serializer.is_valid():
            ingredientes_serializer.save()
            return Response(ingredientes_serializer.data)
        return Response(ingredientes_serializer.errors)
    
    elif request.method == 'DELETE':
        restaurante = Ingredientes.objects.filter(id = pk).first()
        restaurante.delete()
        return Response("El plato del menu ha sido eliminado.")