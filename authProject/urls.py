from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from authApp import views
from productApp import views as viewsProduct
from restaurante import views as viewsRestaurante


urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('product2/', viewsProduct.product_api_view),
    path('product3/<int:pk>', viewsProduct.product_detail_view),
    path('restaurante', viewsRestaurante.restaurante_api_view),
    path('restaurante/<int:pk>', viewsRestaurante.restaurante_detail_view),
    path('ingredientes', viewsRestaurante.ingredientes_api_view),
    path('ingredientes/<int:pk>', viewsRestaurante.ingredientes_detail_view),
]
